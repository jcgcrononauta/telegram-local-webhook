import time, urllib2, json

# CONFIGURATION
bottoken = '' # From Bot Father.
refreshtime = 0.5 # One second
webhookurl = "" # Url that you want to set as telegram webhook.
debug = True
# END CONFIGURATION

apiurl = 'https://api.telegram.org/bot{}'.format(bottoken)

def getUpdates(offset):
    url = apiurl + "/getUpdates?offset=" + str(offset)
    response = urllib2.urlopen(url)
    return response.read()

def localWebhookCall(response):
    if webhookurl == "":
        print "[Telegram Local WebHook]: WebHook isn't configured!"
        return ""
    request = urllib2.Request(webhookurl, response)
    response = urllib2.urlopen(request)

    return response.read()

def setOffset(offset):
    f = open("offset", "w")
    f.write(str(offset))
    
def getOffset():
    f = open("offset", "r")
    return int(f.read())
    
offset = getOffset()

while(1):
    updates = getUpdates(offset)
    json_updates = json.loads(updates)

    if json_updates['ok'] and len(json_updates['result']) > 0:
        update_id = json_updates['result'][0]['update_id']
        for update in json_updates['result']:
            if debug:
                print "[Telegram Local WebHook]: {} updates found!".format(len(json_updates['result']))
            pd = localWebhookCall(json.dumps(update))
            if update_id < update['update_id']:
                update_id = update['update_id']
            if debug:
                print "[Telegram Local WebHook]: Your local webhook has returned the following data: {}".format(pd)
        offset = update_id + 1
        setOffset(offset)
    elif debug:
        print "[Telegram Local WebHook]: No updates at this moment!"
    time.sleep(refreshtime)
